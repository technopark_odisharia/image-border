/*
 ============================================================================
 Name        : image-borders.cu
 Author      : Georgiy Odisharia
 Version     :
 Copyright   : GPL 2
 Description : CUDA compute reciprocals
 ============================================================================
 */

#include <iostream>
#include <fstream>
#include <numeric>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


static void CheckCudaErrorAux (const char *, unsigned, const char *, cudaError_t);
#define CUDA_CHECK_RETURN(value) CheckCudaErrorAux(__FILE__,__LINE__, #value, value)

/**
 * CUDA kernel that computes reciprocal values for a given vector
 */
__global__ void gradientKernel(float *data, float *result, unsigned width, unsigned height) {
	unsigned idx = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned idy = blockIdx.y * blockDim.y + threadIdx.y;

	if((idx < width) && (idy < height)) {
		float g_x = data[idy * (width + 2) + idx]			* 1 	+
					data[idy * (width + 2) + idx + 1]		* 0 	+
					data[idy * (width + 2) + idx + 2]		* (-1) 	+
					data[(idy + 1) * (width + 2) + idx] 	* 2 	+
					data[(idy + 1) * (width + 2) + idx + 1]	* 0 	+
					data[(idy + 1) * (width + 2) + idx + 2] * (-2)	+
					data[(idy + 2) * (width + 2) + idx] 	* 1 	+
					data[(idy + 2) * (width + 2) + idx + 1] * 0 	+
					data[(idy + 2) * (width + 2) + idx + 2] * (-1);
		float g_y = data[idy * (width + 2) + idx]			* 1 	+
					data[idy * (width + 2) + idx + 1]		* 2 	+
					data[idy * (width + 2) + idx + 2]		* 1 	+
					data[(idy + 1) * (width + 2) + idx] 	* 0 	+
					data[(idy + 1) * (width + 2) + idx + 1]	* 0 	+
					data[(idy + 1) * (width + 2) + idx + 2] * 0		+
					data[(idy + 2) * (width + 2) + idx] 	* (-1) 	+
					data[(idy + 2) * (width + 2) + idx + 1] * (-2)	+
					data[(idy + 2) * (width + 2) + idx + 2] * (-1);
		float g =	sqrtf(g_y * g_y + g_x * g_x);
		if (g > 150 && g < 200) {
			result[idy * width + idx] = 255;
		}
		else {
			result[idy * width + idx] = 0;
		}
	}

}

/**
 * Host function that copies the data and launches the work on GPU
 */
float *gpuGradient(float *data, unsigned data_size, unsigned width, unsigned height)
{
	float *result = new float[width*height];
	float *gpuData;
	float *gpuResult;

	CUDA_CHECK_RETURN(cudaMalloc((void **)&gpuData, sizeof(float)*data_size));
	CUDA_CHECK_RETURN(cudaMalloc((void **)&gpuResult, sizeof(float)*width*height));
	CUDA_CHECK_RETURN(cudaMemcpy(gpuData, data, sizeof(float)*data_size, cudaMemcpyHostToDevice));
	
	int N_Threads = 32;
	int N_Blocks_x = 0;
	int N_Blocks_y = 0;
	if ((width % N_Threads) == 0) {
		N_Blocks_x = (width / N_Threads);
	}
	else {
		N_Blocks_x = (width / N_Threads) + 1;
	}
	if (((height) % N_Threads) == 0) {
		N_Blocks_y = (height / N_Threads);
	}
	else {
		N_Blocks_y = (height / N_Threads) + 1;
	}
	dim3 Threads(N_Threads,N_Threads);
	dim3 Blocks(N_Blocks_x,N_Blocks_y);

	cudaEvent_t GPUstart, GPUstop;
	float GPUtime = 0.0f;
	cudaEventCreate(&GPUstart);
	cudaEventCreate(&GPUstop);
	cudaEventRecord(GPUstart, 0);
	gradientKernel<<<Blocks, Threads>>> (gpuData, gpuResult, width, height);
	cudaEventRecord(GPUstop, 0);
	cudaEventSynchronize(GPUstop);
	cudaEventElapsedTime(&GPUtime, GPUstart, GPUstop);
	printf("GPU time : %.3f ms\n", GPUtime);

	CUDA_CHECK_RETURN(cudaMemcpy(result, gpuResult, sizeof(float)*width*height, cudaMemcpyDeviceToHost));
	CUDA_CHECK_RETURN(cudaFree(gpuData));
	CUDA_CHECK_RETURN(cudaFree(gpuResult));
	return result;
}

int main(void)
{
	static const int IMG_WIDTH	= 960;
	static const int IMG_HEIGHT = 512;
	static const std::string filename = "img.text";

	float *raw_array = new float[(IMG_HEIGHT + 2) * (IMG_WIDTH + 2)];
	unsigned int iter;
	for (iter = 0; iter < IMG_WIDTH + 2; iter++ ) {
		raw_array[iter] = 0;
		raw_array[iter + (IMG_WIDTH + 2) * IMG_HEIGHT] = 0;
	}

	std::cout << "I'm here!";

    std::string line;
    std::string delimiter = " ";
    std::string token;
//	std::ifstream raw_file(filename.c_str());
	std::ifstream raw_file("/home/cuda/image-borders/Debug/img.text");
	if (raw_file.is_open()) {
		while(!raw_file.eof())
		{
			getline(raw_file, line);
			raw_array[iter++] = 0;
			size_t pos = 0;
			while ((pos = line.find(delimiter)) != std::string::npos) {
				token = line.substr(0, pos);
				raw_array[iter++] = strtof(token.c_str(), NULL);
				line.erase(0, pos + delimiter.length());
			}
			raw_array[iter++] = strtof(line.c_str(), NULL);
			raw_array[iter++] = 0;
		}
	}
	else {
		std::cout << "Unable to open file" << std::endl << std::endl;
	}

	for (unsigned i = 0; i < IMG_HEIGHT + 2; i++) {
		for (unsigned j = 0; j < IMG_WIDTH + 2; j++) {
			std::cout << raw_array[i * (IMG_WIDTH + 2) + j] << " ";
		}
		std::cout << "\n";
	}
	static const int DATA_SIZE = (IMG_WIDTH + 2) * (IMG_HEIGHT + 2);

//	float *recCpu = cpuReciprocal(data, WORK_SIZE);
	float *out = gpuGradient(raw_array, DATA_SIZE, IMG_WIDTH, IMG_HEIGHT);

	std::ofstream outfile ("/home/cuda/image-borders/Debug/example.txt");
	if (outfile.is_open())
	{
		for (int i = 0; i < IMG_HEIGHT; i++) {
			for(int j = 0; j < IMG_WIDTH; j++){
				outfile << out[j + i*IMG_WIDTH] << " " ;
			}
			outfile << '\n';
		}
		outfile.close();
	}
	else std::cout << "Unable to open file";
	return 0;
}

/**
 * Check the return value of the CUDA runtime API call and exit
 * the application if the call has failed.
 */
static void CheckCudaErrorAux (const char *file, unsigned line, const char *statement, cudaError_t err)
{
	if (err == cudaSuccess)
		return;
	std::cerr << statement<<" returned " << cudaGetErrorString(err) << "("<<err<< ") at "<<file<<":"<<line << std::endl;
	exit (1);
}

